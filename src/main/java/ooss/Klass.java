package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;

    private Student leader;

    private List<Teacher> attachTeachers = new ArrayList<>();
    private List<Student> attachStudents = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Klass)) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (this.equals(student.getKlass())) {
            leader = student;
        } else {
            System.out.println("It is not one of us.");
        }
        for (Teacher attachTeacher : attachTeachers) {
            System.out.println("I am " + attachTeacher.getName() + ", teacher of Class " + number + ". I know " + leader.getName() + " become Leader.");
        }
        for (Student attachStudent : attachStudents) {
            System.out.println("I am " + attachStudent.getName() + ", student of Class " + number + ". I know " + leader.getName() + " become Leader.");
        }
    }

    public boolean isLeader(Student student) {
        return student.equals(leader);
    }

    public void attach(Teacher teacher) {
        attachTeachers.add(teacher);
    }

    public void attach(Student student) {
        attachStudents.add(student);
    }
}
