package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if (klass != null && klass.isLeader(this)) {
            return super.introduce() + String.format(" I am a student. I am the leader of class %d.", klass.getNumber());
        }
        String introduction = super.introduce() + " I am a student.";
        if(klass != null) {
            introduction += String.format(" I am in class %d.", klass.getNumber());
        }
        return introduction;
    }

    public void join(Klass joinKlass) {
        klass = joinKlass;
    }

    public boolean isIn(Klass compareKlass) {
        return compareKlass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }
}
