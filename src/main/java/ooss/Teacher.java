package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private Set<Klass> klasses = new HashSet<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduction = new StringBuilder(super.introduce());
        introduction.append(" I am a teacher.");
        if(klasses.size() > 0) {
            introduction.append(" I teach Class ");
            for (Klass klass : klasses) {
                introduction.append(klass.getNumber()).append(", ");
            }
            introduction.replace(introduction.length() - 2, introduction.length(), ".");
        }
        return introduction.toString();
    }

    public void assignTo(Klass klass) {
        klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klasses.contains(student.getKlass());
    }

}
